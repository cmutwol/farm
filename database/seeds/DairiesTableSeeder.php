<?php

use Illuminate\Database\Seeder;

use App\Dairy;

use Faker\Factory as faker;


class DairiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dairies')->truncate();

        $faker=Faker::create();

        foreach (range(1,50) as $i) {
        	Dairy::create([
        			'number'=>$faker->numberBetween(20,300),
        			'dob'=>mt_rand(2010,2017).'-'.mt_rand(1,12).'-'.mt_rand(1,28),
        			'breed_id'=>$faker->numberBetween(1,4),
        			'status'=>mt_rand(0,1),
              'created_at'=>new DateTime,
              'updated_at'=>new DateTime

        	]);
    }
}
}
