<?php

use Illuminate\Database\Seeder;

class BreedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

     DB::table('breeds')->truncate();

     $breeds=[

     		['id'=>1,'name'=>'Friesian','created_at'=>new DateTime,'updated_at'=>new DateTime],
     		['id'=>2,'name'=>'Guernsey','created_at'=>new DateTime,'updated_at'=>new DateTime],
     		['id'=>3,'name'=>'Jersey','created_at'=>new DateTime,'updated_at'=>new DateTime],
     		['id'=>4,'name'=>'Ayrshire','created_at'=>new DateTime,'updated_at'=>new DateTime]
        ];

        DB::table('breeds')->insert($breeds);
    }
}
