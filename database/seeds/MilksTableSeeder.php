<?php

use Illuminate\Database\Seeder;

use Faker\Factory as faker;

use App\Milk;

class MilksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();

        Milk::truncate();

        foreach (range(1,100) as $key) {
          Milk::create([
              'milking_shift_id'=>$faker->numberBetween(1,2),
              'dairy_id'=>$faker->numberBetween(1,50),
              'milk_qty'=>$faker->numberBetween(1,50)            
          ]);
        }
    }
}
