<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BreedsTableSeeder::class);
        $this->call(DairiesTableSeeder::class);
        $this->call(MilkShiftsTableSeeder::class);
        $this->call(MilksTableSeeder::class);
        $this->call(SuppliersTableSeeder::class);
    }
}
