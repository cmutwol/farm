<?php

use Illuminate\Database\Seeder;

use Faker\Factory as faker;

use App\Supplier;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Supplier::truncate();

      $faker=Faker::create();

      foreach (range(1,20) as $key) {
          Supplier::create([

            'company'=>$faker->company,
            'supplier_number'=>$faker->numberBetween(3289,7894),
            'phone'=>$faker->numberBetween(700000000,799999999),
            'email'=>$faker->email,
            'address'=>$faker->address,
            'created_at'=>new DateTime,
            'updated_at'=>new DateTime
          ]);
      }
    }
}
