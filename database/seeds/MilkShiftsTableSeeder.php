<?php

use Illuminate\Database\Seeder;

use App\MilkShift;

class MilkShiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS=0');

       DB::table('milking_shifts')->truncate();

   $shifts=[

      ['id'=>1,'name'=>'Morning','created_at'=>new DateTime,'updated_at'=>new DateTime],
      ['id'=>2,'name'=>'Everning','created_at'=>new DateTime,'updated_at'=>new DateTime],

      ];

      DB::table('milking_shifts')->insert($shifts);
    }
}
