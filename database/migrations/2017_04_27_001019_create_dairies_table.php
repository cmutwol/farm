<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDairiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dairies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('breed_id')->unsigned()->index();
            $table->foreign('breed_id')
            ->references('id')
            ->on('breeds')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('number');
            $table->date('dob');
            $table->boolean('status');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dairies');
    }
}
