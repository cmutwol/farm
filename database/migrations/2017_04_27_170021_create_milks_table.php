<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMilksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('milking_shift_id')->unsigned()->index();
            $table->foreign('milking_shift_id')
                ->references('id')
                ->on('milking_shifts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('dairy_id')->unsigned()->index();
            $table->foreign('dairy_id')
                ->references('id')
                ->on('dairies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('milk_qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('milks');
    }
}
