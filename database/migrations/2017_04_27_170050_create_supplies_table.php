<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplies', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('supplier_id')->unsigned()->index();
          $table->foreign('supplier_id')
              ->references('id')
              ->on('suppliers')
              ->onDelete('cascade')
              ->onUpdate('cascade');
          $table->date('supply_date');
          $table->date('due_date');
          $table->string('title');
          $table->integer('total');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplies');
    }
}
