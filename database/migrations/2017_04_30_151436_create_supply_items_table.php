<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplyItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supply_id')->unsigned()->index();
            $table->foreign('supply_id')
                  ->references('id')
                  ->on('supplies')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->string('comments');
            $table->integer('fat');
            $table->integer('qty');
            $table->decimal('rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_items');
    }
}
