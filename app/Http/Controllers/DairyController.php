<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Dairy;

use App\Breed;

class DairyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dairies=Dairy::orderBy('created_at','desc')->paginate(10);

        return response()->json([

            'model'=>$dairies

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return response()
          ->json([
              'form' => Dairy::initialize(),
              'option' => [
                  'breeds' => Breed::orderBy('name')->get()
              ]
          ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'breed_id' => 'required',
          'number' => 'required|unique:dairies',
          'dob' => 'required',
          'status' => 'required'
      ]);

      $dairy = Dairy::create($request->all());

      return response()
          ->json([
              'saved' => true
          ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dairy=Dairy::findOrFail($id);

        return $dairy;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $dairy = Dairy::findOrFail($id);

      return response()
          ->json([
              'form' => $dairy,
              'option' => [
                  'breeds' => Breed::orderBy('name')->get()
              ]
          ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
