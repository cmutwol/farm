<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dairy extends Model
{
    protected $fillable=[

      'number',
      'dob',
      'status',

      'breed_id'

    ];

    protected $filter=[

      'id','number','dob','status'
    ];

    public static function initialize()
    {
      return [
        'breed_id'=>'Select',
        'number'=>'',
        'status'=>'',
        'dob'=>date('Y-m-d')
      ];
    }
     public function breed()
     {
         return $this->belongsTo('App\Breed');
     }
}
