<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Milk extends Model
{
    protected $fillable=[

      'milk_shift_id',
      'dairy_id',
      'milk_qty'
    ];

    public function dairy()
    {
      return $this->belongsTo('App\Dairy');
    }

    public function supply()
    {
      return $this->belongsTo('App\supply');
    }

    public function milking_shift()
    {
      return $this->belongsTo('App\MilkingShift');
    }
}
