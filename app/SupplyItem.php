<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplyItem extends Model
{
    public function supplies()
    {
      return $this->belongsTo('App\Supply');
    }
}
