<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supply extends Model
{
    public function milk()
    {
      return $this->hasMany('App\Milk');
    }

    public function supplier()
  {
    return $this->belongsTo('App\Supplier');
  }

  public function supply_items()
  {
    return $this->hasMany('App\SupplyItem');
  }
}
