<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MilkingShift extends Model
{
    public function milks()
    {
      return $this->hasMany('App\Milk');
    }
}
