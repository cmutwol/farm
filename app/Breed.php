<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    public function dairies()
    {
        return $this->hasMany(App\Dairy::class);
    }
}
