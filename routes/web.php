<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'dash'],function()
{
  Route::get('/',function()
  {
    return view('dash.index');
  });

});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/app',function(){

    return response()->json([
      'user'=>[
          'first_name'=>'cyrus',
          'second_name'=>'kip',
      ]
    ]);
});
