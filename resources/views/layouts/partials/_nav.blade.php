<aside class="sidebar-navigation">
    <div class="scrollbar-wrapper">
        <div>
            <button type="button" class="button-menu-mobile btn-mobile-view visible-xs visible-sm">
                <i class="mdi mdi-close"></i>
            </button>

            <!-- Left Menu Start -->
            <ul class="metisMenu nav" id="side-menu">
                <li><a href="{{url('/dash')}}"><i class="ti-home"></i> Dashboard </a></li>

                <li><a href="{{url('/dash/supply')}}"><span class="label label-custom pull-right">11</span> <i class="ti-paint-bucket"></i>Milk Supply </a></li>

                <li>
                    <a href="javascript: void(0);" aria-expanded="false"><i class="ti-light-bulb"></i> Dairy <span class="fa arrow"></span></a>
                    <ul class="nav-second-level nav" aria-expanded="true">
                        <li><a href="#">All dairy</a></li>
                        <li><a href="#">New Dairy</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!--Scrollbar wrapper-->
  </aside>
