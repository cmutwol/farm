
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8" />
        <title>FIS-Farm Insights System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="farm insights system for dairy farmers." name="description" />


        <link rel="shortcut icon" href="/favicon.png">


        <!-- Bootstrap core CSS -->
        <link href="/css/app.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="/assets/css/metisMenu.min.css" rel="stylesheet">
        <!-- Icons CSS -->
        <link href="/assets/css/icons.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="/assets/css/style.css" rel="stylesheet">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
    </head>


    <body>

        <div id="page-wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="">
                        <a href="{{url('/dash')}}" class="logo">
                            <img src="/assets/images/logo.png" alt="logo" class="logo-lg" />
                            <img src="/assets/images/logo_sm.png" alt="logo" class="logo-sm hidden" />
                        </a>
                    </div>
                </div>

                <!-- Top navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">

                            <!-- Mobile menu button -->
                            <div class="pull-left">
                                <button type="button" class="button-menu-mobile visible-xs visible-sm">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <!-- Top nav left menu -->
                            <ul class="nav navbar-nav hidden-sm hidden-xs top-navbar-items">
                                <li><a href="#">FIS-<strong class="text-custom">Alphar*</strong></a></li>
                                {{-- <li><a href="#">Help</a></li>
                                <li><a href="#">Contact</a></li> --}}
                            </ul>

                            <!-- Top nav Right menu -->
                            <ul class="nav navbar-nav navbar-right top-navbar-items-right pull-right">
                                <li class="hidden-xs">
                                    <form role="search" class="navbar-left app-search pull-left">
                                         <input type="text" placeholder="Search..." class="form-control">
                                         <a href=""><i class="fa fa-search"></i></a>
                                    </form>
                                </li>

                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle menu-right-item profile" data-toggle="dropdown" aria-expanded="true"><img src="/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><i class="ti-power-off m-r-10"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end container -->
                </div> <!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- Page content start -->
            <div class="page-contentbar">

                @yield('content')

            </div>
            <!-- end .page-contentbar -->
        </div>
        <!-- End #page-wrapper -->



        <!-- js placed at the end of the document so the pages load faster -->


         <script src="/js/app.js"></script>
         <script src="/assets/js/jquery-2.1.4.min.js"></script>
              <script src="/assets/js/bootstrap.min.js"></script>
              <script src="/assets/js/metisMenu.min.js"></script>
              <script src="/assets/js/jquery.slimscroll.min.js"></script>

              <!-- App Js -->
              <script src="/assets/js/jquery.app.js"></script>


    </body>
</html>
