import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


const router=new VueRouter({


  routes:[
    {path:'/',component:require('./components/dash/Index.vue')},
    //dairy
    {path:'/dairy',component:require('./components/dash/dairy/Index.vue')},
    {path:'/dairy/create',component:require('./components/dash/dairy/Compose.vue')},

    //milk
    {path:'/milk',component:require('./components/dash/production/milk/Index.vue')},
    {path:'/milk/create',component:require('./components/dash/production/milk/Index.vue')},

    //sales
    {path:'/sale',component:require('./components/dash/sale/Index.vue')},
    {path:'/sale/create',component:require('./components/dash/sale/Compose.vue')},

    //supply
    {path:'/supply',component:require('./components/dash/supply/Index.vue')},

    //suppliers
    {path:'/supplier',component:require('./components/dash/supplier/Index.vue')},
    {path:'/supplier/create',component:require('./components/dash/supplier/Compose.vue')}
  ]
})

export default router
